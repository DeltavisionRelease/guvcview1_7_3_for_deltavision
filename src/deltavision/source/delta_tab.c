
#include <glib.h>
#include <glib/gprintf.h>
/* support for internationalization - i18n */
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "globals.h"
#include "callbacks.h"
#include "v4l2uvc.h"
#include "string_utils.h"
#include "vcodecs.h"
#include "video_format.h"
#include  "../../deltasdk/include/dsv_define.h"
#include  "../include/delta_tab.h"
#include  "../../deltasdk/include/dsv_trigger.h"
#include  "../../deltasdk/include/dsv_imu.h"

struct DELTA_DATA delta_data;
struct DELTA_GWIDGET *delta_widgets = NULL;

void  Trigger_Once(GtkWidget *button_Command, struct ALL_DATA * all_data)
{
	ENTER();
	soft_trigger_once(all_data->videoIn->fd);
	g_print("bussname = %s \n", all_data->videoIn->cap.bus_info);
}

void  Trigger_changed(GtkWidget *trigger, 	struct ALL_DATA * all_data)
{
	int index = gtk_combo_box_get_active(trigger);
	g_print("Trigger_changed index = %d \n", index);
	if(index == 0){
		disable_trigger(all_data->videoIn->fd);
	}else{
		enable_trigger(all_data->videoIn->fd);
	}

	if(index == 2 || index == 1)
		gtk_widget_show (delta_widgets->button_Trigger);
	else
		gtk_widget_hide (delta_widgets->button_Trigger);
}

void IMU_Freq_changed(GtkWidget *imu_freq, 	struct DELTA_DATA * delta_data)
{
	int index = gtk_combo_box_get_active(imu_freq);
	g_print("Trigger_changed index = %d \n", index);
}

void Frame_Freq_changed(GtkWidget *imu_freq, 	struct DELTA_DATA * delta_data)
{
	int index = gtk_combo_box_get_active(imu_freq);
	g_print("Trigger_changed index = %d \n", index);
}

void delta_tab(struct ALL_DATA *all_data)
{
	guint product_id = all_data->videoIn->listDevices->listVidDevices[all_data->videoIn->listDevices->current_device].product;
	guint vendor_id = all_data->videoIn->listDevices->listVidDevices[all_data->videoIn->listDevices->current_device].vendor;

	if(getDeviceTypeFromId(vendor_id, vendor_id) == IMG_NOT_DTSV)
		return;
	delta_widgets = g_new0(struct DELTA_GWIDGET, 1);
	memset(&delta_data, 0, sizeof(struct DELTA_DATA));
	delta_data.widgets = delta_widgets;
	delta_data.imu_Closed = FALSE;
	delta_data.debug = DELTA_DEBUG;
	__INIT_MUTEX(&delta_data.imu_mutex);

	struct GLOBAL *global = all_data->global;
	struct vdIn *videoIn = all_data->videoIn;
	struct GWIDGET *gwidget = all_data->gwidget;
	delta_data.videoIn = all_data->videoIn;
	//struct paRecordData *pdata = all_data->pdata;

	GtkWidget *layout;
	GtkWidget *Tab4;
	GtkWidget *Tab4Label;
	GtkWidget *Tab4Icon;

	int line = 0;
	int i = 0;
	VidFormats *listVidFormats;

	//LAYOUT
	layout = gtk_layout_new(NULL, NULL); 
    gtk_widget_show(layout);
	//new hbox for tab label and icon
	Tab4 = gtk_grid_new();
	Tab4Label = gtk_label_new(_("Deltavision"));
	gtk_widget_show (Tab4Label);

	gchar* Tab4IconPath = g_strconcat (PACKAGE_DATA_DIR,"/pixmaps/guvcview/delta_controls.png",NULL);
	//don't test for file - use default empty image if load fails
	//get icon image
	Tab4Icon = gtk_image_new_from_file(Tab4IconPath);
	g_free(Tab4IconPath);
	gtk_widget_show (Tab4Icon);
	gtk_grid_attach (GTK_GRID(Tab4), Tab4Icon, 0, 0, 1, 1);
	gtk_grid_attach (GTK_GRID(Tab4), Tab4Label, 1, 0, 1, 1);

	gtk_widget_show (Tab4);
	gtk_notebook_append_page(GTK_NOTEBOOK(gwidget->boxh),layout,Tab4);

	//TRIGGER  Label
	GtkWidget *label_Trigger = gtk_label_new ("Trigger:");
	gtk_widget_set_size_request(label_Trigger, 80, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Trigger), 0, 0.5);
	gtk_widget_show (label_Trigger);
	
	//Trigger Button
	delta_widgets->button_Trigger = gtk_button_new_with_label("Trigger Once");
	gtk_widget_set_size_request(delta_widgets->button_Trigger, 50, 30);
	gtk_widget_hide (delta_widgets->button_Trigger);
	g_signal_connect (GTK_BUTTON(delta_widgets->button_Trigger), "clicked",
           G_CALLBACK (Trigger_Once), all_data);
	
	//Trigger Comobox
	delta_widgets->combo_Trigger = gtk_combo_box_text_new();
    gtk_widget_set_size_request(delta_widgets->combo_Trigger, 300, 30);
	gtk_widget_set_halign (delta_widgets->combo_Trigger, GTK_ALIGN_FILL);
	gtk_widget_set_hexpand (delta_widgets->combo_Trigger, TRUE);
	//gtk_combo_box_text_prepend (GTK_COMBO_BOX_TEXT(delta_widgets->combo_Trigger), "id_command", "enable");
	gtk_combo_box_text_prepend (GTK_COMBO_BOX_TEXT(delta_widgets->combo_Trigger), "id_imu", "enable");
	gtk_combo_box_text_prepend (GTK_COMBO_BOX_TEXT(delta_widgets->combo_Trigger), "id_none", "disable");
	gtk_combo_box_set_active_id (GTK_COMBO_BOX_TEXT(delta_widgets->combo_Trigger), "id_none");
	gtk_widget_show (delta_widgets->combo_Trigger);
	g_signal_connect (GTK_COMBO_BOX(delta_widgets->combo_Trigger), "changed",
		G_CALLBACK (Trigger_changed), all_data);
	disable_trigger(all_data->videoIn->fd);
	
	//Trigger Box
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start(GTK_BOX(hbox), label_Trigger, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), delta_widgets->combo_Trigger, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), delta_widgets->button_Trigger, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	
	gtk_layout_put (GTK_LAYOUT(layout), hbox, 40,20);
	
	//IMU  Frequency Label
	GtkWidget *label_Imu_freq = gtk_label_new ("IMU Frequency :");
	gtk_widget_set_size_request(label_Imu_freq, 80, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Imu_freq), 0, 0.5);
	gtk_widget_show (label_Imu_freq);

	//IMU  Frequency Comobox
	GtkWidget * combo_IMU_Freq= gtk_combo_box_text_new();
    gtk_widget_set_size_request(combo_IMU_Freq, 300, 30);
	gtk_widget_set_halign (combo_IMU_Freq, GTK_ALIGN_FILL);
	gtk_widget_set_hexpand (combo_IMU_Freq, TRUE);

	gtk_combo_box_text_prepend (GTK_COMBO_BOX_TEXT(combo_IMU_Freq), "id_500", "500hz");
	//gtk_combo_box_text_prepend (GTK_COMBO_BOX_TEXT(combo_IMU_Freq), "id_250", "250hz");
	gtk_combo_box_set_active_id (GTK_COMBO_BOX_TEXT(combo_IMU_Freq), "id_500");
	gtk_widget_show (combo_IMU_Freq);
	g_signal_connect (GTK_COMBO_BOX(combo_IMU_Freq), "changed",
		G_CALLBACK (IMU_Freq_changed), &delta_data);

	//IMU Freq Box
	GtkWidget *imu_freq_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start(GTK_BOX(imu_freq_hbox), label_Imu_freq, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(imu_freq_hbox), combo_IMU_Freq, FALSE, FALSE, 0);
	gtk_widget_show (imu_freq_hbox);

	gtk_layout_put (GTK_LAYOUT(layout), imu_freq_hbox, 40, 70);

	//Frame  Frequency Label
	GtkWidget *label_frame_freq = gtk_label_new ("Trigger Frequency:");
	gtk_widget_set_size_request(label_frame_freq, 80, 30);
	gtk_misc_set_alignment (GTK_MISC (label_frame_freq), 0, 0.5);
	gtk_widget_show (label_frame_freq);

	//Frame  Frequency Comobox
	GtkWidget * combo_Frame_Freq= gtk_combo_box_text_new();
    gtk_widget_set_size_request(combo_Frame_Freq, 300, 30);
	gtk_widget_set_halign (combo_Frame_Freq, GTK_ALIGN_FILL);
	gtk_widget_set_hexpand (combo_Frame_Freq, TRUE);

	//gtk_combo_box_text_prepend (GTK_COMBO_BOX_TEXT(combo_Frame_Freq), "id_40", "40");
	gtk_combo_box_text_prepend (GTK_COMBO_BOX_TEXT(combo_Frame_Freq), "id_30", "30");
	gtk_combo_box_set_active_id (GTK_COMBO_BOX_TEXT(combo_Frame_Freq), "id_30");
	gtk_widget_show (combo_Frame_Freq);
	g_signal_connect (GTK_COMBO_BOX(combo_Frame_Freq), "changed",
		G_CALLBACK (Frame_Freq_changed),& delta_data);

	//Frame Freq Box
	GtkWidget *frame_freq_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start(GTK_BOX(frame_freq_hbox), label_frame_freq, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(frame_freq_hbox), combo_Frame_Freq, FALSE, FALSE, 0);
	gtk_widget_show (frame_freq_hbox);

	gtk_layout_put (GTK_LAYOUT(layout), frame_freq_hbox, 40, 120);

	//IMU INFO
	GtkWidget *label_IMU = gtk_label_new ("IMU INFO");
	gtk_widget_set_size_request(label_IMU, 100, 30);
	gtk_misc_set_alignment (GTK_MISC (label_IMU), 0, 0.5);
	gtk_widget_show (label_IMU);
	gtk_layout_put (GTK_LAYOUT(layout), label_IMU, 40, 170);

	//ACC Label
	GtkWidget *label_ACC = gtk_label_new ("ACC");
	gtk_widget_set_size_request(label_ACC, 100, 30);
	gtk_misc_set_alignment (GTK_MISC (label_ACC), 0.5, 0.5);
	gtk_widget_show (label_ACC);
	
	//ACC Grid
	GtkWidget *grid_ACC = ACC_Grid();

    //Gyro Label
	GtkWidget *label_Gyro = gtk_label_new ("Gyro");
	gtk_widget_set_size_request(label_Gyro, 100, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Gyro), 0.5, 0.5);
	gtk_widget_show (label_Gyro);
	
	//Gyro Grid
	GtkWidget *grid_Gyro= Gyro_Grid();
	
	//ACC Label
	GtkWidget *label_Mag = gtk_label_new ("Mag");
	gtk_widget_set_size_request(label_Mag, 100, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Mag), 0.5, 0.5);
	gtk_widget_show (label_Mag);
	//Mag Grid
	GtkWidget *grid_Mag = Mag_Grid();
	//IMU Grid
	GtkWidget *grid_IMU = gtk_grid_new();
    gtk_widget_show(grid_IMU);
    
	gtk_grid_attach(GTK_GRID(grid_IMU), label_ACC, 0, 0, 1, 1);
	gtk_grid_attach(GTK_GRID(grid_IMU), grid_ACC, 0, 1, 1, 1);
	
	gtk_grid_attach(GTK_GRID(grid_IMU), label_Gyro, 2, 0, 1, 1);
	gtk_grid_attach(GTK_GRID(grid_IMU), grid_Gyro, 2, 1, 1, 1);
	
	gtk_grid_attach(GTK_GRID(grid_IMU), label_Mag, 4, 0, 1, 1);
	gtk_grid_attach(GTK_GRID(grid_IMU), grid_Mag,4, 1, 1, 1);
	
	gtk_layout_put (GTK_LAYOUT(layout), grid_IMU, 60, 220);

	/*------------------ Creating the imu thread ---------------*/
	if( __THREAD_CREATE(&delta_data.IMU_thread, imu_loop, (void *) &delta_data))
	{
		g_printerr("IMU thread creation failed\n");

		ERR_DIALOG (N_("Guvcview error:\n\nUnable to create IMU Thread"),
			N_("Please report it to http:\\www.deltavision.io"),
			&delta_data);
	}
}

void delta_tab_close()
{
	g_print("close delta tab \n");
	if(delta_widgets != NULL){
		g_free(delta_widgets);
		delta_widgets = NULL;
	}

	delta_data.imu_Closed = TRUE;
	__THREAD_JOIN(delta_data.IMU_thread);
	__CLOSE_MUTEX(&delta_data.imu_mutex);
}

GtkWidget * ACC_Grid(void)
{
	GtkWidget *grid_ACC = gtk_grid_new();
    gtk_widget_show(grid_ACC);

	//ACC X label
	GtkWidget *label_ACC_X = gtk_label_new ("x");
	gtk_widget_set_size_request(label_ACC_X, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_ACC_X),0.5, 0.5);
	gtk_widget_show (label_ACC_X);;
	 gtk_grid_attach(GTK_GRID(grid_ACC), label_ACC_X, 0, 0, 1, 1);
	 
	//ACC X value
	 delta_widgets ->label_ACC_X_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_ACC_X_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_ACC_X_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_ACC_X_value);;
	 gtk_grid_attach(GTK_GRID(grid_ACC), delta_widgets ->label_ACC_X_value, 1, 0, 1, 1);


	//ACC Y label
	GtkWidget *label_ACC_Y = gtk_label_new ("y");
	gtk_widget_set_size_request(label_ACC_Y, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_ACC_Y),0.5, 0.5);
	gtk_widget_show (label_ACC_Y);
	 gtk_grid_attach(GTK_GRID(grid_ACC), label_ACC_Y, 0, 1, 1, 1); 
	 
	//ACC Y value
	 delta_widgets ->label_ACC_Y_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_ACC_Y_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_ACC_Y_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_ACC_Y_value);;
	 gtk_grid_attach(GTK_GRID(grid_ACC), delta_widgets ->label_ACC_Y_value, 1, 1, 1, 1);

	 //ACC Z label
	GtkWidget *label_ACC_Z= gtk_label_new ("z");
	gtk_widget_set_size_request(label_ACC_Z, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_ACC_Z),0.5, 0.5);
	gtk_widget_show (label_ACC_Z);;
	 gtk_grid_attach(GTK_GRID(grid_ACC), label_ACC_Z, 0, 2, 1, 1);
	 
	//ACC Z value
	 delta_widgets ->label_ACC_Z_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_ACC_Z_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_ACC_Z_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_ACC_Z_value);;
	 gtk_grid_attach(GTK_GRID(grid_ACC),delta_widgets -> label_ACC_Z_value, 1, 2, 1, 1);

	 return grid_ACC;
}

GtkWidget * Gyro_Grid(void)
{
	GtkWidget *grid_Gyro = gtk_grid_new();
    gtk_widget_show(grid_Gyro);

	//Gyro X label
	GtkWidget *label_Gyro_X = gtk_label_new ("x");
	gtk_widget_set_size_request(label_Gyro_X, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Gyro_X),0.5, 0.5);
	gtk_widget_show (label_Gyro_X);;
	 gtk_grid_attach(GTK_GRID(grid_Gyro), label_Gyro_X, 0, 0, 1, 1);

	//Gyro X value
	 delta_widgets ->label_Gyro_X_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_Gyro_X_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_Gyro_X_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_Gyro_X_value);;
	 gtk_grid_attach(GTK_GRID(grid_Gyro), delta_widgets ->label_Gyro_X_value, 1, 0, 1, 1);

	//Gyro Y label
	GtkWidget *label_Gyro_Y = gtk_label_new ("y");
	gtk_widget_set_size_request(label_Gyro_Y, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Gyro_Y),0.5, 0.5);
	gtk_widget_show (label_Gyro_Y);;
	 gtk_grid_attach(GTK_GRID(grid_Gyro), label_Gyro_Y, 0, 1, 1, 1);

	//Gyro Y value
	 delta_widgets ->label_Gyro_Y_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_Gyro_Y_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_Gyro_Y_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_Gyro_Y_value);;
	 gtk_grid_attach(GTK_GRID(grid_Gyro), delta_widgets ->label_Gyro_Y_value, 1,1, 1, 1);

	 //Gyro Z label
	GtkWidget *label_Gyro_Z= gtk_label_new ("z");
	gtk_widget_set_size_request(label_Gyro_Z, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Gyro_Z),0.5, 0.5);
	gtk_widget_show (label_Gyro_Z);;
	 gtk_grid_attach(GTK_GRID(grid_Gyro), label_Gyro_Z, 0, 2, 1, 1);

	//Gyro X value
	 delta_widgets ->label_Gyro_Z_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_Gyro_Z_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_Gyro_Z_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_Gyro_Z_value);;
	 gtk_grid_attach(GTK_GRID(grid_Gyro), delta_widgets ->label_Gyro_Z_value, 1, 2, 1, 1);

	 return grid_Gyro;
}

GtkWidget * Mag_Grid(void)
{
	GtkWidget *grid_Mag = gtk_grid_new();
    gtk_widget_show(grid_Mag);

	//ACC X label
	GtkWidget *label_Mag_X = gtk_label_new ("x");
	gtk_widget_set_size_request(label_Mag_X, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Mag_X),0.5, 0.5);
	gtk_widget_show (label_Mag_X);;
	 gtk_grid_attach(GTK_GRID(grid_Mag), label_Mag_X, 0, 0, 1, 1);
	 
	//ACC X value
	 delta_widgets ->label_Mag_X_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_Mag_X_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_Mag_X_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_Mag_X_value);
    gtk_grid_attach(GTK_GRID(grid_Mag), delta_widgets ->label_Mag_X_value, 1, 0, 1, 1);

	//ACC Y label
	GtkWidget *label_Mag_Y = gtk_label_new ("y");
	gtk_widget_set_size_request(label_Mag_Y, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Mag_Y),0.5, 0.5);
	gtk_widget_show (label_Mag_Y);;
	 gtk_grid_attach(GTK_GRID(grid_Mag), label_Mag_Y, 0, 1, 1, 1); 
	 
	//ACC Y value
	 delta_widgets ->label_Mag_Y_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_Mag_Y_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_Mag_Y_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_Mag_Y_value);
	gtk_grid_attach(GTK_GRID(grid_Mag), delta_widgets ->label_Mag_Y_value, 1, 1, 1, 1);

	 //ACC Z label
	GtkWidget *label_Mag_Z= gtk_label_new ("z");
	gtk_widget_set_size_request(label_Mag_Z, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (label_Mag_Z),0.5, 0.5);
	gtk_widget_show (label_Mag_Z);;
	 gtk_grid_attach(GTK_GRID(grid_Mag), label_Mag_Z, 0, 2, 1, 1);
	 
	//ACC Z value
	 delta_widgets ->label_Mag_Z_value = gtk_label_new ("0");
	gtk_widget_set_size_request(delta_widgets ->label_Mag_Z_value, 50, 30);
	gtk_misc_set_alignment (GTK_MISC (delta_widgets ->label_Mag_Z_value),0.3, 0.5);
	gtk_widget_show (delta_widgets ->label_Mag_Z_value);
	gtk_grid_attach(GTK_GRID(grid_Mag), delta_widgets ->label_Mag_Z_value, 1, 2, 1, 1);

	 return grid_Mag;
}

void *imu_loop(void *data) {
	struct DELTA_DATA *delta_data = (struct DELTA_DATA *) data;
	g_print("imu loop\n");

    if((delta_data->imu_Fd  = InitExtensionUnit(delta_data->videoIn->cap.bus_info, 1)) == -1) //_CameraEnumeration.DeviceInfo))
    {
    	delta_data->imu_Closed = TRUE;
    	g_print("InitCamera : Extension Unit Initialisation Failed\n");
    }

	float acc_data[3];

	float gyro_data[3];

	float mag_data[3];

	int fresh  = 0;

	while (!delta_data->imu_Closed) {
		if(DELTA_DEBUG)  g_print("GetIMUData \n");

		if( !GetIMUData(delta_data->imu_Fd, acc_data, gyro_data, mag_data) )
		{
			delta_data->imu_Closed = TRUE;
		}
		fresh ++;

		char acc_x_ch[IMU_INFO_LENTH];
		char acc_y_ch[IMU_INFO_LENTH];
		char acc_z_ch[IMU_INFO_LENTH];

		char gro_x_ch[IMU_INFO_LENTH];
		char gro_y_ch[IMU_INFO_LENTH];
		char gro_z_ch[IMU_INFO_LENTH];

		char mag_x_ch[IMU_INFO_LENTH];
		char mag_y_ch[IMU_INFO_LENTH];
		char mag_z_ch[IMU_INFO_LENTH];

		if(fresh > 50){
			fresh = 0;
			sprintf(acc_x_ch, "%9.8f", acc_data[0]);
			sprintf(acc_y_ch, "%9.8f", acc_data[1]);
			sprintf(acc_z_ch, "%9.8f", acc_data[2]);

			sprintf(gro_x_ch, "%9.8f", gyro_data[0]);
			sprintf(gro_y_ch, "%9.8f", gyro_data[1]);
			sprintf(gro_z_ch, "%9.8f", gyro_data[2]);

			sprintf(mag_x_ch, "%9.8f", mag_data[0]);
			sprintf(mag_y_ch, "%9.8f", mag_data[1]);
			sprintf(mag_z_ch, "%9.8f", mag_data[2]);

			gdk_threads_enter();
			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_ACC_X_value),
					acc_x_ch);
			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_ACC_Y_value),
					acc_y_ch);
			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_ACC_Z_value),
					acc_z_ch);

			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_Gyro_X_value),
					gro_x_ch);
			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_Gyro_Y_value),
					gro_y_ch);
			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_Gyro_Z_value),
					gro_z_ch);

			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_Mag_X_value),
					mag_x_ch);
			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_Mag_Y_value),
					mag_y_ch);
			gtk_label_set_text(GTK_LABEL(delta_data->widgets->label_Mag_Z_value),
					mag_z_ch);
			gdk_threads_leave();
			//gdk_flush();
			//sleep(1);
		}
	}

	if(delta_data->imu_Fd != -1){
		close(delta_data->imu_Fd);
		delta_data->imu_Fd = -1;
	}
	delta_data->imu_Closed = TRUE;
	g_print("IMU thread finished \n");
	return;
}
