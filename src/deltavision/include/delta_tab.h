#ifndef DELTA_TAB_H
#define DELTA_TAB_H

#include "guvcview.h"
#include  "../../deltasdk/include/dsv_device.h"

struct DELTA_DATA
{
	struct DELTA_GWIDGET * widgets;
	struct vdIn *videoIn;
	int imu_Closed;
	int imu_Fd;
	int debug;
	__MUTEX_TYPE imu_mutex;
	__THREAD_TYPE IMU_thread;
};

//---------------------delta widget--------------------------------
struct DELTA_GWIDGET
{
	GtkWidget *button_Trigger;
	GtkWidget *combo_Trigger ;

	GtkWidget *label_ACC_X_value ;
	GtkWidget *label_ACC_Y_value ;
	GtkWidget *label_ACC_Z_value ;

	GtkWidget *label_Gyro_X_value ;
	GtkWidget *label_Gyro_Y_value ;
	GtkWidget *label_Gyro_Z_value ;

	GtkWidget *label_Mag_X_value ;
	GtkWidget *label_Mag_Y_value ;
	GtkWidget *label_Mag_Z_value ;

};
//------------------------- delta Tab ---------------------------------
void delta_tab(struct ALL_DATA *all_data);
void delta_tab_close();
void *imu_loop(void *data);

GtkWidget * ACC_Grid(void);
GtkWidget * Gyro_Grid(void);
GtkWidget * Mag_Grid(void);
#endif
