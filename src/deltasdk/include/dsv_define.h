/*
 * dsv_define.h
 *
 *  Created on: 2018年6月13日
 *      Author    : allen
 *     Vendor    : www.deltavision.io
 */

#ifndef DSV_DEFINE_H_
#define DSV_DEFINE_H_

#include "stdint.h"

#ifndef QWORD
	typedef uint64_t QWORD;
#endif

#ifndef DWORD
	typedef uint32_t DWORD;
#endif

#ifndef WORD
	typedef uint16_t WORD;
#endif

#ifndef BYTE
	typedef uint8_t  BYTE;
#endif

#ifndef LONG
	typedef unsigned int LONG;
#endif

#ifndef ULLONG
	typedef unsigned long long ULLONG;
#endif

#ifndef ULONG
	typedef unsigned long      ULONG;
#endif

#ifndef UINT
	typedef unsigned int UINT;
#endif

#ifndef pchar
	typedef char* pchar;
#endif

#ifndef INT8
	typedef int8_t     INT8;
#endif

#ifndef UINT8
	typedef uint8_t    UINT8;
#endif

#ifndef INT16
	typedef int16_t    INT16;
#endif

#ifndef UINT16
	typedef uint16_t   UINT16;
#endif

#ifndef INT32
	typedef int32_t    INT32;
#endif

#ifndef UINT32
	typedef uint32_t   UINT32;
#endif

#ifndef INT64
	typedef int64_t    INT64;
#endif

#ifndef UINT64
	typedef uint64_t   UINT64;
#endif

#ifndef TRUE
#define		TRUE	1
#define 		FALSE	0
#endif

#define DELTA_DEBUG 0
#define ENTER()  if(DELTA_DEBUG) {g_print("enter->%s \n", __FUNCTION__);}
#define END()  if(DELTA_DEBUG) {g_print("end->%s \n", __FUNCTION__);}

#endif /*DSV_DEFINE_H_ */
