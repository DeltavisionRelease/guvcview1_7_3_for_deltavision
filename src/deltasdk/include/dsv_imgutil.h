/*
 * dsv_imgutil.h
 *
 *  Created on: 2018年6月3日
 *   Author       : allen
 */

#ifndef DSV_IMGUTIL_H
#define DSV_IMGUTIL_H

#include "dsv_define.h"

void bayer16_convert_dual_bayer8(uint8_t* in_bytes, uint8_t* out_bytes, int width, int height);
void bayer_mono_to_rgb24(BYTE *pBay, BYTE *pRGB24, int width, int height, int pix_order);
void bayer16_convert_bayer8(int16_t *inbuf, uint8_t *outbuf, int width, int height, int shift);

#endif /* DSV_IMGUTIL_H */
