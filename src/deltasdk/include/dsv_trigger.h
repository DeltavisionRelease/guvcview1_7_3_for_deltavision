/*
 * dsv_trigger.h
 *
 *  Created on: 2018年6月3日
 *   Author       : allen
 */

#ifndef DSV_TRIGGER_H_
#define DSV_TRIGGER_H_

int open_v4l2_device(char *device_name);
void enable_trigger(int v4l2_dev);
void disable_trigger(int v4l2_dev);
void soft_trigger_once(int v4l2_dev);


#endif /* DSV_TRIGGER_H_ */
