/*
 * dsv_device.h
 *
 *  Created on: 2018年6月3日
 *      Author    : allen
 *     Vendor    : www.deltavision.io
 */
#ifndef  DSV_DEVICE_H_
#define DSV_DEVICE_H_

#ifndef	 TRUE
	#define		TRUE	    1
	#define 		FALSE	0
#endif

/* deltavision camera type*/
typedef enum
{
	IMG_NOT_DTSV = -1,
	IMG_OTHER_DTSV = 0,
	IMG_MT9V034 = 0x0004,
	IMG_M031 = 0x0005,
	IMG_OV7251 = 0x0014,
	IMG_AR0144 = 0x002A,
	IMG_M034 = 0x002B,
	IMG_OV9281 = 0x002D,
} dsv_device_type;


typedef enum
{
	GBRG = 0,
	GRBG,
	RGBG,
	BGGR
} pixel_order;

/* deltavision feature structure*/
typedef struct
{
	dsv_device_type img_type;
	pixel_order pixl_order;
	unsigned int pixel_width;
	int is_mono;
	int imu_support;
	int double_width;
	int is_stereo;
}dtsv_specific;


dsv_device_type getDeviceTypeFromId(int vendor_id, int product_id);
void getDTSVSpecific(dsv_device_type type, dtsv_specific * specific );
void getDTSVSpecificFromId(int vendor_id, int product_id, dtsv_specific * specific );

#endif /* DSV_DEVICE_H_ */
